using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEditor;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public static event Action OnStart;

    public GameObject bread;
    public GameObject tomato;
    public GameObject salad;
    public GameObject egg;

    public string[] ingredients = { "Null", "Bread", "Tomato", "Salad", "Egg"};

    public string[,] ingredientMatrix;
    public int x = 4;
    public int y = 4;

    private void Start()
    {
        OnStart?.Invoke();    
    }
    public void SetCameraPosition(int[,] matrix)
    {
        float maxX = 0;
        int app = 3;

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                if (matrix[i, j] != 0)
                {
                    if(j < app)
                    app = j;

                    maxX = i;
                }
            }
        }
        if(maxX > 2)
        {
            Camera.main.fieldOfView = 114;
        }
        else
        {
            Camera.main.fieldOfView = 102;
        }

        Vector3 cameraPosUpdated = Camera.main.transform.position;
        cameraPosUpdated.x = maxX / 2;

        cameraPosUpdated.z = app - 0.5f;

        Camera.main.transform.position = cameraPosUpdated;
    }
}