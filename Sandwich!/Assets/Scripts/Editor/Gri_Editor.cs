using log4net.Core;
using PlasticGui.WorkspaceWindow.PendingChanges;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Printing;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.TerrainTools;
using UnityEngine;

[CustomEditor(typeof(Grid))]
public class Gri_Editor : Editor
{
    int[,] intMatrix;
    int[,] intMatrixSpawned;

    bool canSpawn = false;

    private void OnEnable()
    {
        serializedObject.Update();    
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("GRID");
        EditorGUILayout.Space();

        Grid grid = (Grid)target;
        DrawGrid(grid);

        ResetGrid(grid);

        if(canSpawn)
            InstantiateIngredient(grid);

        if(GUI.changed)
        {
            grid.SetCameraPosition(intMatrix);
            canSpawn = true;
        }
    }

    private void DrawGrid(Grid grid)
    {
        if (grid.ingredientMatrix == null)
        {
            grid.ingredientMatrix = new string[grid.x, grid.y];
        }

        if(intMatrix == null)
        {
            intMatrix = new int[grid.x, grid.y];
            intMatrixSpawned = new int[grid.x, grid.y];
        }

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.Width(60)); 

        for (int i = 0; i < grid.x; i++)
        {
            EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(65));
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginVertical();

        for (int i = 3; i >= 0; i--)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(35));

            for (int j = 0; j < grid.x; j++)
            { 
                intMatrix[j, i] = EditorGUILayout.Popup(intMatrix[j, i], grid.ingredients, GUILayout.Width(65));
                grid.ingredientMatrix[j, i] = grid.ingredients[intMatrix[j, i]];
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }

    private void InstantiateIngredient(Grid grid)
    {
        for (int x = 0; x < grid.x; x++)
        {
            for (int z = 0; z < grid.y; z++)
            {
                if (grid.ingredientMatrix[z, x] != "Null")
                {
                    switch(grid.ingredientMatrix[z, x])
                    {
                        case "Bread":
                            if (intMatrixSpawned[z, x] == 1)
                            {
                                DestroyIngredient(x, z);
                            }

                            Instantiate(grid.bread, new Vector3(z, 0,x), Quaternion.identity);
                            canSpawn = false;
                            intMatrixSpawned[z, x] = 1;
                            break;
                        case "Salad":
                            if (intMatrixSpawned[z, x] == 1)
                            {
                                DestroyIngredient(x, z);
                            }

                            Instantiate(grid.salad, new Vector3(z, 0, x), Quaternion.identity);
                            canSpawn = false;
                            intMatrixSpawned[z, x] = 1;
                            break;
                        case "Tomato":
                            if (intMatrixSpawned[z, x] == 1)
                            {
                                DestroyIngredient(x, z);
                            }

                            Instantiate(grid.tomato, new Vector3(z, 0, x), Quaternion.identity);
                            canSpawn = false;
                            intMatrixSpawned[z, x] = 1;
                            break;
                        case "Egg":
                            if (intMatrixSpawned[z, x] == 1)
                            {
                                DestroyIngredient(x, z);
                            }

                            Instantiate(grid.egg, new Vector3(z, 0, x), Quaternion.identity);
                            canSpawn = false;
                            intMatrixSpawned[z, x] = 1;

                            break;
                    }
                }
                if (grid.ingredientMatrix[z, x] == "Null")
                {
                    DestroyIngredient(x, z);
                }
            }
        }
    }

    private void DestroyIngredient(int x, int z)
    {
        RaycastHit hit;
        if (Physics.Raycast(new Vector3(z, 1, x), Vector3.down, out hit, 1))
        {
            if (hit.collider != null)
            {
                intMatrixSpawned[z, x] = 0;
                DestroyImmediate(hit.transform.gameObject);
            }
        }
    }

    private void ResetGrid(Grid grid)
    {
        GUILayout.Space(15);

        if (GUILayout.Button("Reset Grid", GUILayout.Width(90), GUILayout.Height(20)))
        {
            for(int i = 0; i < 4; i++)
            {
                for(int j = 0; j < 4; j++)
                {
                    DestroyIngredient(i, j);
                    grid.ingredientMatrix[i, j] = "Null";
                    intMatrix[i, j] = 0;
                    intMatrixSpawned[i, j] = 0;
                }
            }

            intMatrix = null;
            intMatrixSpawned = null;
            grid.ingredientMatrix = null;
        }
    }
}
