using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class UndoSystem : MonoBehaviour
{
    UndoSystem instance;

    public static Vector3 previousPosition;
    public static Ingredient ingredientToMove;
    public static Vector3 direction;

    public static List<Ingredient> childs = new List<Ingredient>();

    private bool buttonClicked = false;

    private void OnEnable()
    {
        Ingredient.OnIngredientSelected += LoadChild;
    }
    private void OnDisable()
    {
        Ingredient.OnIngredientSelected -= LoadChild;
    }

    private void Start()
    {
        print(InputSystem.canSwipe);
        print(GameManager.win);
        print(buttonClicked);
    }

    public void Undo()
    {
        if (InputSystem.canSwipe && !GameManager.win && !buttonClicked)
        {
            MoveIngredient();
            UnParentIngredient();
            EnableCollider();

            buttonClicked = true;
            GameManager.placedIngredients --;
        }
    }

    private void LoadChild(Ingredient ingredient)
    {
        childs.Clear();

        if (ingredient.transform.childCount > 0)
        {
            Ingredient ing = ingredient;

            for (int i = 0; i < ingredient.transform.childCount; i++)
            {
                childs.Add(ingredient.transform.GetChild(i).GetComponent<Ingredient>());
            }

            ingredientToMove = ingredient;
            previousPosition = ingredient.transform.position;
        }
        else
        {
            ingredientToMove = ingredient;
            previousPosition = ingredient.transform.position;
        }
    }
    private void MoveIngredient()
    {
        for(int i = 0; i < childs.Count; i++)
        {
            childs[i].transform.parent = ingredientToMove.transform;
        }

        ingredientToMove.transform.DOJump(previousPosition, 1, 1, 0.5f).OnComplete(ResetStats);
        RotateIngredient();  
    }
    private void RotateIngredient()
    {
        if (direction == Vector3.right)
        {
            ingredientToMove.transform.DOLocalRotate(new Vector3(0, 0, 180), 0.5f, RotateMode.WorldAxisAdd);
        }
        if (direction == Vector3.left)
        {
            ingredientToMove.transform.DOLocalRotate(new Vector3(0, 0, -180), 0.5f, RotateMode.WorldAxisAdd);
        }
        if (direction == Vector3.forward)
        {
            ingredientToMove.transform.DOLocalRotate(new Vector3(-180, 0, 0), 0.5f, RotateMode.WorldAxisAdd);
        }
        if (direction == Vector3.back)
        {
            ingredientToMove.transform.DOLocalRotate(new Vector3(180, 0, 0), 0.5f, RotateMode.WorldAxisAdd);
        }
    }
    private void UnParentIngredient()
    {
        ingredientToMove.transform.parent = null;
    }
    private void EnableCollider()
    {
        ingredientToMove.GetComponent<Collider>().enabled = true;
    }
    private void ResetStats()
    {
        previousPosition = Vector3.zero;
        ingredientToMove = null;
        direction = Vector3.zero;
        buttonClicked = false;
        childs.Clear();
    }
}
