using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Unity.VisualScripting;

public class InputSystem : MonoBehaviour
{
    public static InputSystem instance;

    public static event Action OnGameStarted;

    Vector2 beginPos, endPos;

    public Ingredient ingredient1;
    public Ingredient ingredient2;

    public static List<Transform> ingredients = new List<Transform>();

    public static Ingredient prevIng;
    [SerializeField] Vector3 prevIngPos;

    Vector3[] directions = {Vector3.up, Vector3.down, Vector3.right, Vector3.left};

    public static bool canSwipe = true;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if(Input.touchCount == 1)
        {
            if (!GameManager.gameStarted)
            {
                GameManager.gameStarted = true;
                OnGameStarted?.Invoke();
            }

            TouchTypes();
        }
    }

    private void TouchTypes()
    {
        Touch touch = Input.GetTouch(0);

        if (canSwipe)
        {
            if (touch.phase == TouchPhase.Began)
            {
                TouchBegan(touch);
            }

            if (touch.phase == TouchPhase.Ended)
            {
                TouchEnd(touch);
            }
        }
    }

    private void TouchBegan(Touch touch)
    {
        beginPos = touch.position;  
        
        Ray ray = Camera.main.ScreenPointToRay(beginPos);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if(hit.collider.tag == "Ingredient")
            {
                ingredient1 = hit.transform.gameObject.GetComponent<Ingredient>();
            }
        }
    }

    private void TouchEnd(Touch touch)
    {
        if (ingredient1 != null)
        {
            endPos = touch.position;

            Vector3 direction = GetTrueDir();
            direction.z = direction.y;
            direction.y = 0;

            RaycastHit hit;

            if(Physics.Raycast(ingredient1.transform.position, direction, out hit, Mathf.Infinity))
            {
                if (hit.collider.tag == "Ingredient")
                {
                    ingredient2 = hit.transform.gameObject.GetComponent<Ingredient>();
                }
            }

            if (ingredient2 != null)
            {
                ingredient1.Move(ingredient2, ingredient2.transform.position, direction);
            }
            else
            {
                canSwipe = false;
                ingredient1.WrongAnimation(direction);
            }

            ingredient1 = null;
            ingredient2 = null;
        }
    }

    private Vector3 GetTrueDir()
    {
        float trueDistance = float.MaxValue;
        Vector3 trueDir = Vector3.zero;

        Vector3 dist = endPos - beginPos;

        foreach (Vector3 dir in directions)
        {
            float distance = Vector3.Distance(dist, dir);

            if (distance < trueDistance)
            {
                trueDistance = distance;
                trueDir = dir;
            }
        }

        return trueDir;
    }
}
