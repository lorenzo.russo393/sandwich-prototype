using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient : MonoBehaviour
{
    public static event Action<Ingredient> OnIngredientSelected;
    public static event Action OnSetParent;

    Ingredient nextIngredient;

    [SerializeField] bool isBread;
    [SerializeField] bool bottomBread;
    [SerializeField] bool topBread;
    [SerializeField] bool breadInStack;

    private Vector3 startingPos;

    public void Move(Ingredient ingredientOnGround, Vector3 ingredientOnGroundPos, Vector3 direction)
    {
        UndoSystem.direction = direction;

        if ((this.isBread && ingredientOnGround.bottomBread && GameManager.placedIngredients == GameManager.ingredientsInGame) ||
            (this.bottomBread && ingredientOnGround.isBread && GameManager.placedIngredients == GameManager.ingredientsInGame) ||
            (!this.isBread && ingredientOnGround.isBread) || (!this.isBread && !ingredientOnGround.isBread) ||
            (!this.bottomBread && ingredientOnGround.bottomBread && GameManager.placedIngredients == GameManager.ingredientsInGame))
        {
            InputSystem.canSwipe = false;

            OnIngredientSelected?.Invoke(this);

            nextIngredient = ingredientOnGround;

            Vector3 rotationDirection = new Vector3(direction.z, 0, -direction.x);
            Vector3 rotation = (rotationDirection * 180);

            float childs = 0;

            if (CheckThisChildrens() >= CheckNextIngredientChildrens())
            {
                childs = CheckThisChildrens() + (childs + (CheckNextIngredientChildrens() + 1));

                if (CheckNextIngredientChildrens() == CheckThisChildrens() && CheckNextIngredientChildrens() != 0 && CheckThisChildrens() != 0)
                {

                    childs = ((CheckThisChildrens()) + (1 * CheckThisChildrens() + 1));
                }
            }
            else
            {
                childs = CheckNextIngredientChildrens() + (childs + (CheckThisChildrens() + 1));

                if (CheckNextIngredientChildrens() == CheckThisChildrens() && CheckNextIngredientChildrens() != 0 && CheckThisChildrens() != 0)
                {
                    childs = ((CheckThisChildrens()) + (1 * CheckThisChildrens()));
                }
            }

            if ((direction == Vector3.forward || direction == Vector3.back) && (ingredientOnGroundPos.z == transform.position.z + 1 ||
                ingredientOnGroundPos.z == transform.position.z - 1)
                && ingredientOnGroundPos.x == transform.position.x)
            {
                InputSystem.instance.ingredient1 = null;

                Vector3 offset = new Vector3(0, 0.2f * (childs), 0);

                transform.DOJump(ingredientOnGroundPos + offset, 1, 1, 0.5f).OnComplete(DeParentNextIng);
                //transform.DORotate(new Vector3(180, 0, 0), 0.5f).OnComplete(AddParent);

                transform.DOLocalRotate(rotation, 0.5f, RotateMode.WorldAxisAdd).OnComplete(AddParent);
            }
            if ((direction == Vector3.right || direction == Vector3.left) && (ingredientOnGroundPos.x == transform.position.x + 1 ||
                ingredientOnGroundPos.x == transform.position.x - 1)
                && ingredientOnGroundPos.z == transform.position.z)
            {
                InputSystem.canSwipe = false;
                InputSystem.instance.ingredient1 = null;

                Vector3 offset = new Vector3(0, 0.2f * (childs), 0);

                transform.DOJump(ingredientOnGroundPos + offset, 1, 1, 0.5f).OnComplete(DeParentNextIng);
                //transform.DORotate(new Vector3(0, 0, 180), 0.5f).OnComplete(AddParent);
                transform.DOLocalRotate(rotation, 0.5f, RotateMode.WorldAxisAdd).OnComplete(AddParent);
            }
        }
        else
        {
            InputSystem.canSwipe = false;
            WrongAnimation(direction);
        }
    }

    public void WrongAnimation(Vector3 direction)
    {
        InputSystem.instance.ingredient1 = null;
        startingPos = transform.position;
        this.transform.DOPunchPosition(direction * 0.05f, 0.5f, 8, 1).OnComplete(ResetPosition);
    }

    private void ResetPosition()
    {
        this.transform.DOMove(startingPos, 0.01f);
        InputSystem.canSwipe = true;
    }

    private int CheckThisChildrens()
    {
        int totchild = 0;

        foreach(Transform child in transform)
        {
            totchild++;
        }

        return totchild;
    }

    private int CheckNextIngredientChildrens()
    {
        int totchild = 0;

        if (nextIngredient.transform.childCount > 0)
        {
            foreach (Transform child in nextIngredient.transform)
            {
                totchild++;
            }

        }
        return totchild;
    }

    private void DeParentNextIng()
    {
        List<Transform> childrens = new List<Transform>();

        foreach (Transform child in transform)
        {
            childrens.Add(child);
        }

        foreach (Transform child in childrens)
        {
            child.parent = nextIngredient.transform;
        }
    }

    private void AddParent()
    {
        transform.parent = nextIngredient.transform;
        transform.GetComponent<Collider>().enabled = false;

        if(nextIngredient.isBread)
        {
            nextIngredient.bottomBread = true;
        }

        OnSetParent?.Invoke();
        RecalculateCollider();
        //UndoSystem.ingredient = nextIngredient;
        //UndoSystem.previousPosition = this.transform.position;     
    }

    private void RecalculateCollider()
    { 
        float y = (CheckNextIngredientChildrens() + 1) / 2;
        float size = CheckNextIngredientChildrens() + 1;

        nextIngredient.GetComponent<BoxCollider>().center = new Vector3(0, y, 0);
        nextIngredient.GetComponent<BoxCollider>().size = new Vector3(1, size, 1);

        InputSystem.canSwipe = true;
    }
}