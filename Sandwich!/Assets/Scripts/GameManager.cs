using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject winUI;

    public static int ingredientsInGame;
    public static int placedIngredients;

    public static bool win = false;

    [Header("Tutorial stuff")]
    [SerializeField] UnityEngine.UI.Image pointClick;
    public static bool gameStarted = false;
    private Vector3 initPointerPosition;
    bool pointerVisible = false;

    private void Start()
    {
        initPointerPosition = pointClick.transform.position;
        InputSystem.canSwipe = true;
        win = false;
        placedIngredients = 0;
        GameObject[] ingredients = GameObject.FindGameObjectsWithTag("Ingredient");
        ingredientsInGame = ingredients.Length - 2;

       StartCoroutine(TutorialAnimation());
    }

    private void OnEnable()
    {
        Ingredient.OnSetParent += CheckWinRate;
        InputSystem.OnGameStarted += DeleteCursor;
    }

    private void OnDisable()
    {
        Ingredient.OnSetParent -= CheckWinRate;
        InputSystem.OnGameStarted -= DeleteCursor;
    }

    private IEnumerator TutorialAnimation()
    {
        yield return new WaitForSeconds(1);

        if (!gameStarted)
        {
            var alpha = pointClick.color;

            while(alpha.a < 255)
            {
                alpha.a += Time.deltaTime;
                pointClick.color = alpha;
            }

            yield return new WaitForSeconds(0.2f);

            while (!gameStarted)
            {
                int randomDirection = Random.Range(1, 3);

                print(randomDirection);

                if (randomDirection == 1)
                {
                    pointClick.transform.DOMoveY(initPointerPosition.y + 300, 1f).OnComplete(ResetPosition);
                }
                else
                {
                    pointClick.transform.DOMoveX(initPointerPosition.x - 300, 1f).OnComplete(ResetPosition);
                }

                yield return new WaitForSeconds(2);
            }
        }
    }

    private void ResetPosition() => pointClick.transform.position = initPointerPosition;
    private void DeleteCursor() => pointClick.gameObject.SetActive(false);

    private void CheckWinRate()
    {
        placedIngredients++;

        if (placedIngredients == ingredientsInGame + 1)
        {
            winUI.SetActive(true);
            win = true;
            InputSystem.canSwipe = false;
        }
    }
}
